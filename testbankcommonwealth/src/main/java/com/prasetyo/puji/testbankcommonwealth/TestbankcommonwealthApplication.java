package com.prasetyo.puji.testbankcommonwealth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestbankcommonwealthApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestbankcommonwealthApplication.class, args);
	}

}
