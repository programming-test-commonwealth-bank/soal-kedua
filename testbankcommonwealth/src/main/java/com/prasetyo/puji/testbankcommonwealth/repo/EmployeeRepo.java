package com.prasetyo.puji.testbankcommonwealth.repo;

import java.util.Optional;

import com.prasetyo.puji.testbankcommonwealth.entity.Employee;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Long> {
    Optional<Employee> findByName(String name);
}