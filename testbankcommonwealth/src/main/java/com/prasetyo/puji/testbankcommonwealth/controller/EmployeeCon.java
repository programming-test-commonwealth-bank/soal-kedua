package com.prasetyo.puji.testbankcommonwealth.controller;

import java.util.Optional;

import com.prasetyo.puji.testbankcommonwealth.entity.Employee;
import com.prasetyo.puji.testbankcommonwealth.repo.EmployeeRepo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/employee")
public class EmployeeCon {

    @Autowired
    EmployeeRepo employeeRepo;

    @GetMapping("")
    ResponseEntity<Iterable<Employee>> getAll() {
        try {
            return new ResponseEntity<>(this.employeeRepo.findAll(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("")
    ResponseEntity<Employee> createOne(@RequestBody Employee employee) {
        try {
            Optional<Employee> result = this.employeeRepo.findByName(employee.getName());
            if (result.isPresent()) {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            } else {
                return new ResponseEntity<>(this.employeeRepo.save(employee), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/{id}")
    ResponseEntity<Employee> updateOne(@RequestBody Employee employee, @PathVariable Long id) {
        try {
            Optional<Employee> result = this.employeeRepo.findById(id);
            if (result.isPresent()) {
                result.get().setName(employee.getName());
                result.get().setPhone(employee.getPhone());
                this.employeeRepo.save(result.get());
                return new ResponseEntity<>(result.get(), HttpStatus.CREATED);
            } else {
                return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Employee> deleteOne(@PathVariable Long id) {
        try {
            Optional<Employee> result = this.employeeRepo.findById(id);
            if (result.isPresent()) {
                this.employeeRepo.delete(result.get());
                return new ResponseEntity<>(result.get(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

}